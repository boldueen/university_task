import sys


def char_to_int(c: str) -> str | int:
    try:
        c_as_int = int(c)
        return c_as_int
    except:
        return "NOT AN INTEGER"


def main():
    try:
        input_arg = sys.argv[1]
    except IndexError:
        print("INPUT ONE VARIABLE!")
        return
    print(char_to_int(input_arg))


if __name__ == "__main__":
    main()
