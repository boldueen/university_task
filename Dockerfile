FROM python:3.11
COPY requirements.txt requirements.txt
RUN pip install -r requirements.txt
COPY task_2.py task_2.py 
CMD ["python", "task_2.py"]