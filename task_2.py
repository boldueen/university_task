import redis

data = """
subject,student,result
math,John,3
math,Sara,5
math,Alex,5
math,Albert,5
"""

r = redis.Redis("redis", 6379)


def init_data():
    r.set("data", data)


def get_data():
    result = r.get("data")
    return result


if __name__ == "__main__":
    init_data()
    result = get_data()
    print(result)
